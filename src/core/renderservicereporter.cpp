#include "renderservicereporter.h"

#include <sstream>

int RenderServiceReporter::m_reportId = 0;
RenderServiceReporter RenderServiceReporter::m_instance = RenderServiceReporter();

const std::string RenderServiceReporter::OutputHeader    = "##RenderService##";
const std::string RenderServiceReporter::Parsing         = "Parsing";
const std::string RenderServiceReporter::Rendering       = "Rendering";
const std::string RenderServiceReporter::WritingImage    = "WritingImage";

///////////////////////////////////////////////////////////////////////////////

/*static*/
RenderServiceReporter RenderServiceReporter::GetInstance() {
    return RenderServiceReporter::m_instance;
}

///////////////////////////////////////////////////////////////////////////////

RenderServiceReporter::RenderServiceReporter()
    : pOutStream(stdout) {
}

///////////////////////////////////////////////////////////////////////////////

RenderServiceReporter::~RenderServiceReporter() {
}

///////////////////////////////////////////////////////////////////////////////

std::string IntToString(int i) {
    std::stringstream ss;
    std::string s;
    ss << i;
    s = ss.str();

    return s;
}

///////////////////////////////////////////////////////////////////////////////

void RenderServiceReporter::Report(const std::string strMessage) {
    // Report format:
    //  <output header>:<report id>:<status message>
    //
    std::string strReportMessage = RenderServiceReporter::OutputHeader;
    strReportMessage.append(":");
    strReportMessage.append(IntToString(RenderServiceReporter::m_reportId));
    strReportMessage.append(":");
    strReportMessage.append(strMessage);
    strReportMessage.append("\n");
    printf(strReportMessage.c_str());

    RenderServiceReporter::m_reportId++;
}