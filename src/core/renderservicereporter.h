#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_CORE_RENDERSERVICEREPORTER_H
#define PBRT_CORE_RENDERSERVICEREPORTER_H

#include "pbrt.h"

class RenderServiceReporter {
private:
    static int m_reportId;
    static RenderServiceReporter m_instance;
public:
    static const std::string OutputHeader;
    static const std::string Parsing;
    static const std::string Rendering;
    static const std::string WritingImage;
public:
    static RenderServiceReporter GetInstance();
private:
    RenderServiceReporter();
public:
    ~RenderServiceReporter();
    void Report(const std::string strMessage);
private:
    FILE* pOutStream;
};

#endif // PBRT_CORE_RENDERSERVICEREPORTER_H