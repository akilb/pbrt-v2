#include "ResourceMonitor.h"


HANDLE ResourceMonitor::m_hThread = NULL;

///////////////////////////////////////////////////////////////////////////////

/*static*/
void ResourceMonitor::Initialise() {
    ResourceMonitor::m_hThread = CreateThread(NULL, 0, monitoringThreadTask, NULL, 0, NULL);
    SetThreadPriority(ResourceMonitor::m_hThread, THREAD_MODE_BACKGROUND_BEGIN);
}

///////////////////////////////////////////////////////////////////////////////

/*static*/
void ResourceMonitor::Shutdown() {
    TerminateThread(ResourceMonitor::m_hThread, 0);
}

///////////////////////////////////////////////////////////////////////////////

DWORD WINAPI ResourceMonitor::monitoringThreadTask(LPVOID arg) {
    ResourceMonitor monitor;

    while(true) {
        monitor.MeasureResourceUsage();
        monitor.ReportResources();

        Sleep(2000);
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

ResourceMonitor::ResourceMonitor() : m_iRenderId(PbrtOptions.renderId) {
    InitialisePerfCounters();
    InitialiseNamedPipe();
}

///////////////////////////////////////////////////////////////////////////////

ResourceMonitor::~ResourceMonitor() {
    CloseHandle(m_hPipe);
}

///////////////////////////////////////////////////////////////////////////////
/*static*/
void ResourceMonitor::InitialisePerfCounters() {
    // Initialise perf counters
    PDH_STATUS hr;

    if (FAILED(hr = PdhOpenQuery(NULL, 0, &m_hPerformanceQuery)) ||
        FAILED(hr = PdhAddCounter(m_hPerformanceQuery,
                                  L"\\Processor(_Total)\\% Processor Time",
                                  0,
                                  &m_hCpuCounter))) {
        // TODO: Error reporting...
        printf("Failed to initiase perf counters: %lx\n", hr);
        return;
    }

    // Store initial perf counter data now since we need at least two values to
    // calculate CPU usage.
    if (FAILED(hr = PdhCollectQueryData(m_hPerformanceQuery))) {
        printf("PdhCollectQueryData: %lx\n", hr);
    }
}

///////////////////////////////////////////////////////////////////////////////
/*static*/
void ResourceMonitor::InitialiseNamedPipe() {
    const LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\rendermonitorpipe");

    m_hPipe = CreateFile(lpszPipename,        // pipe name 
                        PIPE_ACCESS_OUTBOUND, // read and write access
                        0,                    // no sharing 
                        NULL,                 // default security attributes
                        OPEN_EXISTING,        // opens existing pipe 
                        0,                    // default attributes 
                        NULL);                // no template file 
 
    if (m_hPipe != INVALID_HANDLE_VALUE) {
        return;
    }

    // The only failure we tolerate is the pipe being busy...
    if (GetLastError() != ERROR_PIPE_BUSY) 
    {
        printf("Could not open named pipe. [Error=%d]\n", GetLastError() );
        return;
    }

    // All pipe instances are busy, so wait for 20 seconds for one to
    // become available..
    if (!WaitNamedPipe(lpszPipename, 20000))
    { 
        printf("Could not open pipe: 20 second wait timed out."); 
        return;
    }
}

///////////////////////////////////////////////////////////////////////////////

void ResourceMonitor::MeasureResourceUsage() {
    PDH_STATUS hr;

    // Store all perf counter data
    if (FAILED(hr = PdhCollectQueryData(m_hPerformanceQuery))) {
        printf("PdhCollectQueryData %lx\n", hr);
        return;
    }
 
    if (FAILED(hr = PdhGetFormattedCounterValue(m_hCpuCounter, 
                                                PDH_FMT_LONG, 
                                                0, 
                                                &m_cpuCounterVal))) {
        printf("PdhGetFormattedCounterValue(cpu) %lx\n", hr);    
        return;
    }

    //printf("cpu %3d%%\n", m_cpuCounterVal.longValue); 
}

///////////////////////////////////////////////////////////////////////////////

void ResourceMonitor::ReportResources() {
    if (m_hPipe == INVALID_HANDLE_VALUE) {
        // Not connected so don't report.
        return;
    }

    // Send a message to the pipe server.
    WCHAR strMsg[100];
    DWORD bytesToWrite;
    DWORD bytesWritten;
    swprintf(strMsg, L"%i:%d\n", m_iRenderId, m_cpuCounterVal.longValue);
    bytesToWrite = (lstrlen(strMsg)+1)*sizeof(TCHAR);

    if (!WriteFile(m_hPipe,         // pipe handle 
                  strMsg,           // message 
                  bytesToWrite,     // message length 
                  &bytesWritten,    // bytes written 
                  NULL)) {          // not overlapped
        printf("WriteFile to pipe failed. [GLE=%d]\n", GetLastError());
        return;
    }
}
