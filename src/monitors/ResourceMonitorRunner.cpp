#include "ResourceMonitor.h"
#include "ResourceMonitorRunner.h"


ResourceMonitorRunner::ResourceMonitorRunner() {
    ResourceMonitor::Initialise();
}

///////////////////////////////////////////////////////////////////////////////

ResourceMonitorRunner::~ResourceMonitorRunner() {
    ResourceMonitor::Shutdown();
}