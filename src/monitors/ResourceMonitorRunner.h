#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_MONITORS_RESOURCEMONITORRUNNER_H
#define PBRT_MONITORS_RESOURCEMONITORRUNNER_H

class ResourceMonitorRunner {
public:
    ResourceMonitorRunner();
    ~ResourceMonitorRunner();
};

#endif // PBRT_MONITORS_RESOURCEMONITORRUNNER_H