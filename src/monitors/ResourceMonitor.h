#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_MONITORS_RESOURCEMONITOR_H
#define PBRT_MONITORS_RESOURCEMONITOR_H

#include "pbrt.h"

#if defined(PBRT_IS_WINDOWS)
#include <pdh.h>
#include <windows.h>
#endif

#define FAILED(hr) (hr != ERROR_SUCCESS)

class ResourceMonitor {
private:
    static HANDLE m_hThread;

    // Perf counters
    PDH_HQUERY m_hPerformanceQuery;
    PDH_HCOUNTER m_hCpuCounter;
    PDH_FMT_COUNTERVALUE m_cpuCounterVal;

    // Named Pipe values
    HANDLE m_hPipe;
    int m_iRenderId;

public:
    static void Initialise();
    static void Shutdown();

    ~ResourceMonitor();

private:
    static DWORD WINAPI monitoringThreadTask(LPVOID arg);

    ResourceMonitor();

    void InitialisePerfCounters();
    void InitialiseNamedPipe();
    void MeasureResourceUsage();
    void ReportResources();
};

#endif // PBRT_MONITORS_RESOURCEMONITOR_H